# Sentiment Neuron on Yelp Review Classification
This is a fork of https://github.com/openai/generating-reviews-discovering-sentiment. I added visualizations on top of that codebase and also included code from some forks downstream.

I've added an [example](Example_Usage.ipynb) notebook showing how to use this as a module.  

## Installing as a package
Run the following command:
```
pip install git+https://gitlab.com/aaroncwhite/generating-reivews-discovering-sentiment#egg=sentiment-neuron
```

I'm hoping you have a virtual environment activated by then!

## Installing for testing
There's a [docker](Dockerfile) definition that can be used to test and run the code exactly as I developed it. Run either with `docker-compose` or `docker` directly:
```
docker-compose build
docker-compose up 
```

or

```
docker build -t sentiment_neuron .
docker run -p 9888:8888 sentiment_neuron
```

By default, the container launches a jupyter instance on top of the tensorflow gpu images provided.  

### Environment Variables
If using `docker-compose`, you should specify at least a `RUNTIME` environment in a `.env` file.  See the [example](example.env) for the format.  I have this configured so I can switch between GPU and non-GPU development machines.  

| variable | values |
| ----     | ----   |
| RUNTIME  | `nvidia`, `runc` |
| NVIDIA_VISIBLE_DEVICES | comma separated ids if multiple devices, or just `0` if only one available |


Set `RUNTIME=nvidia` for `nvidia-docker` and `RUNTIME=runc` for the default, non-GPU enabled, runtime.  This will influence the GPU's visibility within the container. 

NOTE: `docker-compose` maps jupyter's default port to `9888`, not `8888`. 

You could also use `pipenv` directly if you're confident you have your GPU configuration correct.  I've run into so many problems with native Tensorflow installations on Ubuntu that I strictly use the Docker route at this point. 

```
pipenv install --dev
pipenv run jupyter lab
```
