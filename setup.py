#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages
from ast import literal_eval
import os

DEV = literal_eval(os.environ.get("DEV", "0"))


def read(*names, **kwargs):
    try:
        with io.open(
            join(dirname(__file__), *names),
            encoding=kwargs.get('encoding', 'utf8')
        ) as fh:
            return fh.read()
    except:
        return ''

readme = read('README.md')
history = read('HISTORY.md')

requirements = ['pandas',
                'numpy', 
                'tensorflow<=1.14.0', 
                'tqdm',
                'scikit-learn',
                'matplotlib',
                'seaborn', 
                'progressbar2',
                'setuptools_scm']

setup_requirements = [

                      ]

test_requirements = ['pytest', ]

setup(
    author="Aaron White",
    author_email='aaroncwhite@gmail.com',
    classifiers=[
        'Development Status :: Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    description="Package implementation of OpenAI's sentiment neuron",
    install_requires=requirements,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='sentiment analysis',
    name='sentiment_neuron',
    packages=['sentiment_neuron'],
    use_scm_version={"root": ".", "relative_to": __file__} if not DEV else False,
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://github.com/aaroncwhite/generating-reviews-discovering-sentiment',
    zip_safe=False,
)